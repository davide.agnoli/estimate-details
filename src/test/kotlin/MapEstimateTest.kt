import org.http4k.core.Method
import org.http4k.core.Request
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MapEstimateTest {
    @Test
    fun `deserialize json object`() {
        val jsonData = """
        {
          "customerDetails" : {
            "surname" : "Smtih",
            "title" : "Mr",
            "addressingName" : "Mr Smith",
            "contactMethod" : "EMAIL",
            "addressLine1" : "171",
            "addressLine2" : "Westminster",
            "postcode" : "SW1E 5NN",
            "email" : "me.you@johnlewis.co.uk"
          },
          "estimates" : [
            {
            "caseCode" : "FC1773",
            "workRoom" : "FC",
            "branchId" : "039",
            "branchName" : "John Lewis Kingston",
            "estimateNo" : "17731773_1",
            "estimateAmount" : 177300,
            "orderable" : "true",
            "paid" : "false",
            "closed" : "false",
            "accepted" : "true",
            "acceptedDate" : "2020-01-16T16:22:00Z"
          }
          ]
        }
        """

        val result = MapEstimate().map(jsonData)

        assertEquals(
            result,
null
        )
    }
    @Test
    fun `serialize json object`() {
        val jsonData = ""
    }
    @Test
    fun `test call`() {
        val result = createApp()(Request(Method.GET, "/get-estimates"))
        assertEquals(result, "")
    }
}