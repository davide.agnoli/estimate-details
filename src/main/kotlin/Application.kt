import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.http4k.client.ApacheClient
import org.http4k.core.*
import org.http4k.core.Status.Companion.OK
import org.http4k.format.Jackson.auto
import org.http4k.lens.string
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Netty
import org.http4k.server.asServer

fun main() {

    val port = 9000
    createApp()
        .asServer(Netty(port))
        .start()

    println("App running on http://localhost:$port")

}

fun createApp() =
    routes(
        "get-estimates" bind Method.GET to ::getCPMEstimates,
        "get-resources" bind Method.GET to ::getAppointmentsFromEta,
    )


fun getCPMEstimates(request: Request): Response {
    val apiKey =
        "apikey"
    val client = ApacheClient()
    val cpmRequest = Request(
        Method.GET,
        "https://hfa4.appiancloud.com/suite/webapi/hpp-customer-estimates?postcode=E6+1BG&estimateNo=17731773_1"
    ).header(
        name = "Appian-API-Key",
        value = apiKey
    )
    val CPMreponse = MapEstimate().map(client(cpmRequest).bodyString())
    val estimatedetresp: List<EstimateDetailsResponse> = CPMreponse.estimates.map {
        EstimateDetailsResponse(
            caseCode = it.caseCode,
            estimateNo = it.estimateNo,
            estimateAmount = it.estimateAmount
        )
    }

    val apiResponse = EstimateResponse(CPMreponse.customerDetails.surname,
        CPMreponse.customerDetails.email,
        CPMreponse.estimates.size,
        estimatedetresp

    )
    val responseShape = Body.auto<EstimateResponse>().toLens()
    return Response(OK).with(
        responseShape of apiResponse
    )

    //return client(cpmRequest)
}
fun EstimateDetails.toEstimateDetailsResponse() = EstimateDetailsResponse(
    caseCode = "$caseCode",
    estimateNo= "$estimateNo",
    estimateAmount= estimateAmount
)
fun getAppointmentsFromEta(request: Request): Response {
    val apiKey =
        "apikey"
    val client = ApacheClient()
    val cpmRequest = Request(
        Method.GET,
        "https://api.etadirect.com/rest/ofscCore/v1/activities/?resources=Estimators&dateFrom=2021-04-10&dateTo=2021-04-20&q=status=='pending'&q=activityType=='Measure'&q=A_PROVIDER_DEPARTMENT=='Floor  Coverings'&fields=activityId,activityType,resourceId,date,timeOfBooking,startTime,endTime,duration,timeSlot,status,A_TITLE,customerName,A_FIRST_NAME,A_LAST_NAME,A_ADDR_PREMISE_STREET,A_ADDR_TOWN,A_ADDR_CITY,city,A_ADDR_COUNTY,postalCode,customerEmail,customerCell,customerPhone,timeSlot,workZone,A_PROVIDER_DEPARTMENT,A_PRODUCTS,A_TASK_LIST,activityType,A_BOOKING_NOTES"
    ).header(
        name = "Appian-API-Key",
        value = apiKey
    )
    val apiresponse = MapEstimate().map(client(cpmRequest).bodyString())

    val responseShape = Body.auto<CPMEstimateRequest>().toLens()
    return Response(OK).with(
        responseShape of apiresponse
    )

}

data class CustomerDetails(
    val surname: String,
    val title: String,
    val addressingName: String,
    val contactMethod: String,
    val addressLine1: String,
    val addressLine2: String,
    val postcode: String,
    val email: String
)

data class EstimateDetails(
    val caseCode: String,
    val workRoom: String,
    val branchId: String,
    val branchName: String,
    val estimateNo: String,
    val estimateAmount: Double,
    val orderable: Boolean,
    val paid: Boolean,
    val closed: Boolean,
    val accepted: Boolean,
    val acceptedDate: String
)
data class EstimateDetailsResponse(
    val caseCode: String,
    val estimateNo: String,
    val estimateAmount: Double,
)

data class EstimateResponse(
    val surname: String,
    val email: String,
    val numberOfEstimates: Int,
    val estimates: List<EstimateDetailsResponse>
)

data class CPMEstimateRequest(
    val customerDetails: CustomerDetails,
    val estimates: List<EstimateDetails>,
)

class MapEstimate {
    val mapper: ObjectMapper = ObjectMapper()
        .registerKotlinModule()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

    fun map(jsonbody: String): CPMEstimateRequest {
        return mapper.readValue(jsonbody)
    }
}



